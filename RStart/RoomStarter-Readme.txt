
#####################################################
#####################################################
# Created By:       Shay Pasvolsky                  #
# Gitlab:           @shipser                        #
# Email:            shaypas@gmail.com               #
# Creation Date:    sep 20, 2020                    #
# Last Update:      sep 28, 2020                    #
# License:          GNU GPLv3                       #
#####################################################
#####################################################

Script Version: 1.31

Usage:
  RoomStarter [optional Command...]

Available Commands:
  -h, --help      Help for RoomStarter
  -r              Room Name (Must be set if not running the help)
  -f              Folder path reletive to "/home/user" (Must exists befor hand, Must not include the room`s name)
  -F              Full folder path (Must exists befor hand, Must not include the room`s name)
  -n              Do not create readme.md file for the room
  -v              Print the script version number and exit
  -p              Re-generate the script`s readme file
  -s              Small room - only readme file will be created
  -t              Sets the number of tasks in the room

Examples:
  RoomStarter -h                                   ->  See this help
  RoomStarter --help                               ->  See this help
  RoomStarter -r "Room Name"                       ->  Create the Room`s Folder and substructure with defualt base folder
  RoomStarter -r "Room Name" -f "Folder Name"      ->  Create the Room`s Folder and substructure with user`s folder reletive to base folder
  RoomStarter -r "Room Name" -F "Folder Path"      ->  Create the Room`s Folder and substructure with user`s folder full path
  RoomStarter -r "Room Name" -n                    ->  Create the Room`s Folder and substructure with defualt base folder without creating the readme.md file
  RoomStarter -r "Room Name" -s                    ->  Create the Room`s Readme file only (Room_name.md)
  RoomStarter -r "Room Name" -t NUM                ->  Create the Room`s Readme file with Num (number) of Tasks specified

Be Advised:
      Folders must be owned by the user running ths script. Root User and sudo command cann`t run this script for safty reasons.
      The Use is on your own reask, make sure to understand what it is doing before you use it.


The readme.md template for the room is set by RoomStarter file. It must exists in the same location as this script.
If you want the room`s name in the output readme.md file, use "New_Room_name" in the Template where you want it.
If you want the start date and time added to the readme file, use "DTSaver" in the Template where you want it.
If you want your IP in the output readme file, use "MY-IP-NUM" in the Template where you want it.
If you want to specify the tasks template build it and use "T_NUM" in the template where you want the task`s template to begin. tasks must be the last thing on the template file.


Known bugs:
      # FIXME: No Known Errors and Bugs

TODO:
*****
      # TODO: Build the system for the config file:
      #       1. Set a var for the config file name
      #       2. Set a defualt build for the config file
      #       3. Test if the config file exists, if not create a default one
      #       4. Build a read file function
      #       5. Test to see if the config file has all the needed params, if not set the miising to defaults
      #       6. Load the configuration and run
      # TODO: End of config system build.
      #
      # TODO: Config Contents:
      #                      Move the list of sub folders to a config file
      #                      Move the readme.md template file location and name to a config file
      #                      Move the Base Folder value to a config file
      #                      Move the String to replace with the room`s name to a config file
      #                      Move the String to replace with the date and time to a config file
      # TODO: Config Contents end.

License is in 'License'!!


##############
# Changelog: #
##############

# Changes from v1.3 to v1.31
****************************

# Configuration changes:
        # Did some cleanup in the folder structure

# Changes from v1.22 to v1.3
****************************

# New Features:
        # Added the abilty to specify how many tasks in the room for the readme file

# Changes from v1.21 to v1.22
****************************

# Fixses: 
        # Fixed the creation of directories if nested and parent is not present for the rooms main directory

# Changes from v1.2 to v1.21
****************************

# Fixses: 
        # Fixed the creation of directories if nested and parent is not present
        # Fixed some typos in texts

# Changes from v1.1 to v1.2
***************************

# Fixses: 
        # Fixed spaces in -f and -F input leading to stopping the script and giving the user an error and help menu

# New Features:
        # Added support for creating only readme file
        # Added Changlog output
        # Added the option to add users ip in the readme template



###################################
# The script consists of 4 files: #
###################################

1. The readme (this file - RoomStarter-Readme.txt)
2. The license (license)
3. Tamplate for room`s readme (RoomStarter)
4. The script itself (RoomStarter)
